//
//  CameraControll.m
//  ImageRecognitionFramework
//
//  Created by Aleksandar Mitrovski on 6/5/18.
//  Copyright © 2018 Aleksandar Mitrovski. All rights reserved.
//
@import MobileCoreServices;
#import "CameraControll.h"

@implementation CameraControll

@synthesize imagePicker;
@synthesize isGallery;
@synthesize holder;

#pragma mark Initialization

-(id) init {
    if (self = [super init]) {
        [self initImagePicker];
    }
    return self;
}

-(void) initImagePicker {
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
}

#pragma mark options for image picking

/**
 * Open the camera
 * @param vc (UIViewController) The View Controller from which the camera will be launched
 */
-(void) openCameraOnViewController: (UIViewController *) vc {
    isGallery = NO;
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        holder = vc;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        NSLog(@"%@", imagePicker);
        [vc presentViewController:imagePicker animated:YES completion:NULL];
    }
}

/**
 * Open the photo library
 * @param vc (UIViewController) The View Controller from which the photo lib will be launched
 */
-(void) openPhotoLibOnViewController: (UIViewController *) vc {
    isGallery = YES;
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary]) {
        holder = vc;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [vc presentViewController:imagePicker animated:YES completion: NULL];
    }
}

#pragma mark Image Picker Delegate Methods

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"**  HERE WE ARE");
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    if (!isGallery) {
        UIImageWriteToSavedPhotosAlbum(image, self,  @selector(image:finishedSavingWithError:contextInfo:),nil);
    }
    [picker removeFromParentViewController];
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        
        if (SYSTEM_VERSION_LESS_THAN(@"9.0")) {
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: @"Save failed"
                                  message: @"Failed to save image"
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        } else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Save failed" message:@"Failed to save image" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                 {
                                     //BUTTON OK CLICK EVENT
                                 }];
            [alert addAction:ok];
            [holder presentViewController:alert animated:YES completion:nil];
        }
        
    } else {
        
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    NSLog(@"##  HERE WE ARE");
}

@end
