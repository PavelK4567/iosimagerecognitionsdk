//
//  GalleryViewController.m
//  ImageRecognitionFramework
//
//  Created by Aleksandar Mitrovski on 6/12/18.
//  Copyright © 2018 Aleksandar Mitrovski. All rights reserved.
//

#import "GalleryViewController.h"

@interface GalleryViewController () {
    NSMutableArray *imagesInGallery;
    NSMutableArray *selectedImages;
    NSMutableArray *galleryButtons;
    bool imageSelectMode;
    
    UIButton *delete;
    UIButton *share;
    
    UIImageView *fullScreen;
    UIButton *cancelSelected;
    UIButton *back;
    
    NSString *dataPath;
    UIScrollView *scroll;
    
    UILongPressGestureRecognizer* longPressGesture;
}

@end

@implementation GalleryViewController

@synthesize lang = _lang;

- (void)viewDidLoad {
    [super viewDidLoad];
    imagesInGallery = [[NSMutableArray alloc] init];
    imageSelectMode = NO;
    [self setGestures];
    galleryButtons = [[NSMutableArray alloc] init];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    selectedImages = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getAllImagesFromLocation];
    [self buildHeader];
    [self clearGallery];
    [self buildTheGalleryWithTotalFiles];
}

-(void) setGestures {
    longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(startSelectingImages:)];
    [longPressGesture setMinimumPressDuration:0.85];
}

-(UILongPressGestureRecognizer *) getLongPressGesture {
    UILongPressGestureRecognizer * longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(startSelectingImages:)];
    [longPress setMinimumPressDuration:1.5];
    return longPress;
}

#pragma mark -
#pragma mark build the header view
-(void) getAllImagesFromLocation {
    NSFileManager *manager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    dataPath = [documentsDirectory stringByAppendingPathComponent:@"/MyFolder"];
    
    //imagesInGallery = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:&error];
    NSArray *tmp = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:&error];
    if (error) {
        NSLog(@"FAILED TO LOAD IMAGES FROM LOCATION :: %@", dataPath);
        NSLog(@"%@", error);
    }
    
    if ([manager fileExistsAtPath:[NSString stringWithFormat:@"%@/%@", dataPath, [self getNameForDefImage:@"apple"]]]) {
        [imagesInGallery addObject:[self getNameForDefImage:@"apple"]];
    }
    
    if ([manager fileExistsAtPath:[NSString stringWithFormat:@"%@/%@", dataPath, [self getNameForDefImage:@"bee"]]]) {
        [imagesInGallery addObject:[self getNameForDefImage:@"bee"]];
    }
    
    if ([manager fileExistsAtPath:[NSString stringWithFormat:@"%@/%@", dataPath, [self getNameForDefImage:@"beer"]]]) {
        [imagesInGallery addObject:[self getNameForDefImage:@"beer"]];
    }
    
    if ([manager fileExistsAtPath:[NSString stringWithFormat:@"%@/%@", dataPath, [self getNameForDefImage:@"kite"]]]) {
        [imagesInGallery addObject: [self getNameForDefImage:@"kite"]];
    }
    
    if ([manager fileExistsAtPath:[NSString stringWithFormat:@"%@/%@", dataPath, [self getNameForDefImage:@"sunflower"]]]) {
        [imagesInGallery addObject:[self getNameForDefImage:@"sunflower"]];
    }
    
    for (int i = 0;i < tmp.count; i++) {
        if (([[tmp objectAtIndex:i] rangeOfString:[self getNameForDefImage:@"apple"]].location == NSNotFound) &&
            ([[tmp objectAtIndex:i] rangeOfString:[self getNameForDefImage:@"bee"]].location == NSNotFound) &&
            ([[tmp objectAtIndex:i] rangeOfString:[self getNameForDefImage:@"beer"]].location == NSNotFound) &&
            ([[tmp objectAtIndex:i] rangeOfString:[self getNameForDefImage:@"kite"]].location == NSNotFound) &&
            ([[tmp objectAtIndex:i] rangeOfString:[self getNameForDefImage:@"sunflower"]].location == NSNotFound)) {
            
            [imagesInGallery addObject:[tmp objectAtIndex:i]];
            
        }
    }
    NSLog(@"##### %@", imagesInGallery);
}

-(NSString *) getNameForDefImage: (NSString *) defName {
    if ([_lang isEqualToString:@"en"]) {
        if ([defName isEqualToString:@"apple"]) {
            return @"_apple.jpg";
        } else if ([defName isEqualToString:@"bee"]) {
            return @"_bee.jpg";
        } else if ([defName isEqualToString:@"beer"]) {
            return @"_beer.jpg";
        } else if ([defName isEqualToString:@"kite"]) {
            return @"_kite.jpg";
        } else if ([defName isEqualToString:@"sunflower"]) {
            return @"_sunflower.jpg";
        } else {
            return @"";
        }
        
    } else if ([_lang isEqualToString:@"it"]) {
        if ([defName isEqualToString:@"apple"]) {
            return @"_mela.jpg";
        } else if ([defName isEqualToString:@"bee"]) {
            return @"_ape.jpg";
        } else if ([defName isEqualToString:@"beer"]) {
            return @"_birra.jpg";
        } else if ([defName isEqualToString:@"kite"]) {
            return @"_aquilone.jpg";
        } else if ([defName isEqualToString:@"sunflower"]) {
            return @"_girasole.jpg";
        } else {
            return @"";
        }
    } else if ([_lang isEqualToString:@"fr"]) {
        if ([defName isEqualToString:@"apple"]) {
            return @"_pomme.jpg";
        } else if ([defName isEqualToString:@"bee"]) {
            return @"_abeille.jpg";
        } else if ([defName isEqualToString:@"beer"]) {
            return @"_bière.jpg";
        } else if ([defName isEqualToString:@"kite"]) {
            return @"_Cerf-volant.jpg";
        } else if ([defName isEqualToString:@"sunflower"]) {
            return @"_tournesol.jpg";
        } else {
            return @"";
        }
    } else if ([_lang isEqualToString:@"es"]) {
        if ([defName isEqualToString:@"apple"]) {
            return @"_manzana.jpg";
        } else if ([defName isEqualToString:@"bee"]) {
            return @"_Abeja.jpg";
        } else if ([defName isEqualToString:@"beer"]) {
            return @"_Cerveza.jpg";
        } else if ([defName isEqualToString:@"kite"]) {
            return @"_Cometa.jpg";
        } else if ([defName isEqualToString:@"sunflower"]) {
            return @"_Girasol.jpg";
        } else {
            return @"";
        }
    } else {
        if ([defName isEqualToString:@"apple"]) {
            return @"_apple.jpg";
        } else if ([defName isEqualToString:@"bee"]) {
            return @"_bee.jpg";
        } else if ([defName isEqualToString:@"beer"]) {
            return @"_beer.jpg";
        } else if ([defName isEqualToString:@"kite"]) {
            return @"_kite.jpg";
        } else if ([defName isEqualToString:@"sunflower"]) {
            return @"_sunflower.jpg";
        } else {
            return @"";
        }
    }
}



-(void) buildHeader {
    UIView *v = [[UIView alloc] init];
    [v setBackgroundColor:[UIColor whiteColor]];
    [v setFrame:CGRectMake(0, 0, self.view.frame.size.width, 70)];
    
    cancelSelected = [[UIButton alloc] initWithFrame:CGRectMake(5, 20, 30, 30)];
    [self setImage:@"close_img" forButton:cancelSelected];
    cancelSelected.hidden = YES;
    [cancelSelected.titleLabel setFont:[UIFont systemFontOfSize:40.0]];
    [cancelSelected setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelSelected addTarget:self action:@selector(clearSelected) forControlEvents:UIControlEventTouchUpInside];
    [v addSubview:cancelSelected];
    
    back = [[UIButton alloc] initWithFrame:CGRectMake(5, 20, 30, 30)];
    [self setImage:@"back_black_img" forButton:back];
    [back addTarget:self action:@selector(backPressed) forControlEvents:UIControlEventTouchUpInside];
    [v addSubview:back];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(v.frame.size.width/2 - 100, 20, 200, 35)];
    title.textAlignment = NSTextAlignmentCenter;
    [title setText:@"Galeria"];
    [title setFont:[UIFont systemFontOfSize:20.0]];
    [title setTextColor:[UIColor darkGrayColor]];
    [v addSubview:title];
    
    delete = [[UIButton alloc] initWithFrame:CGRectMake(v.frame.size.width - 55, 10, 50, 50)];
    delete.hidden = YES;
    [self setImage:@"delete_black_img" forButton:delete];
    [delete addTarget:self action:@selector(deletePressed) forControlEvents:UIControlEventTouchUpInside];
    [v addSubview:delete];
    
    share = [[UIButton alloc] initWithFrame:CGRectMake(v.frame.size.width - 100, 10, 50, 50)];
    share.hidden = YES;
    [self setImage:@"share_black_img" forButton:share];
    [share addTarget:self action:@selector(sharePressed) forControlEvents:UIControlEventTouchUpInside];
    [v addSubview:share];
    
    [self.view addSubview:v];
    
}
#pragma mark build gallery
-(void) buildTheGalleryWithTotalFiles {
    NSUInteger total = [imagesInGallery count];
    scroll = [[UIScrollView alloc] init];
    [scroll setFrame:CGRectMake(0, 70, self.view.frame.size.width, self.view.frame.size.height - 70)];
    
    int groupNum = (int)ceil(total / 3.0);
    
    float usableW = self.view.frame.size.width - 20;
    float imgW = usableW / 3.0;
    float imgH = imgW / 0.562;
    [scroll setShowsHorizontalScrollIndicator:NO];
    
    for (int i = 0; i < groupNum; i++) {
        int startIndex = i * 3;
        UIView *holder = [[UIView alloc] init];
        [holder setFrame:CGRectMake(0, i * (imgW + 10) , self.view.frame.size.width, imgW + 10)];
        
        UIButton *imgL = [[UIButton alloc] init];
        imgL.tag = startIndex;
        [self setGalleryImage:[imagesInGallery objectAtIndex:startIndex] forButton:imgL withFrame:CGRectMake(5, 0, imgW, imgW)];
        [imgL setFrame:CGRectMake(5, 0, imgW, imgW)];
        [imgL addGestureRecognizer:[self getLongPressGesture]];
        [imgL addTarget:self action:@selector(imageTapped:) forControlEvents:UIControlEventTouchUpInside];
        [holder addSubview: imgL];
        [galleryButtons addObject:imgL];
        
        if (startIndex + 1 < total) {
            UIButton *imgM = [[UIButton alloc] init];
            imgM.tag = startIndex + 1;
            [imgM addGestureRecognizer:[self getLongPressGesture]];
            [self setGalleryImage:[imagesInGallery objectAtIndex:startIndex + 1] forButton:imgM withFrame:CGRectMake(imgW + 10, 0, imgW, imgW)];
            [imgM setFrame:CGRectMake(imgW + 10, 0, imgW, imgW)];
            [imgM addTarget:self action:@selector(imageTapped:) forControlEvents:UIControlEventTouchUpInside];
            [holder addSubview: imgM];
            [galleryButtons addObject:imgM];
        }
        
        if (startIndex + 2 < total) {
            UIButton *imgR = [[UIButton alloc] init];
            imgR.tag = startIndex + 2;
            [imgR addGestureRecognizer:[self getLongPressGesture]];
            [self setGalleryImage:[imagesInGallery objectAtIndex:startIndex + 2] forButton:imgR withFrame:CGRectMake(2 * imgW + 15, 0, imgW, imgW)];
            [imgR setFrame:CGRectMake(2 * imgW + 15, 0, imgW, imgW)];
            [imgR addTarget:self action:@selector(imageTapped:) forControlEvents:UIControlEventTouchUpInside];
            [holder addSubview: imgR];
            [galleryButtons addObject:imgR];
        }
        
        [scroll addSubview:holder];
    }
    [scroll setContentSize:CGSizeMake(self.view.frame.size.width, groupNum * (imgW + 10))];
    
    [self.view addSubview:scroll];
}

- (void) setImage: (NSString *) name forButton: (UIButton *) btn {
    UIImage *img = [UIImage imageNamed:name inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    
    [btn setImage:img forState:UIControlStateNormal];
    [btn setImage:img forState:UIControlStateSelected];
    [btn setImage:img forState:UIControlStateHighlighted];
}

- (void) setGalleryImage: (NSString *) name forButton: (UIButton *) btn withFrame: (CGRect) rect {
    UIImage *img = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", dataPath, name]];
    NSLog(@"name: %@, size: h = %f, w = %f", name, img.size.height, img.size.width);
    if (img.size.width > 800) {
        img = [self croppIngimageByImageName:img toRect:CGRectMake(img.size.width/2 - 400, img.size.height/2 - 400, 800, 800)];
    } else {
        img = [self croppIngimageByImageName:img toRect:CGRectMake(img.size.width/2 - 200, img.size.height/2 - 200, 400, 400)];
    }
    [btn setContentMode:UIViewContentModeScaleAspectFill];
    [btn setImage:img forState:UIControlStateNormal];
    [btn setImage:img forState:UIControlStateSelected];
    [btn setImage:img forState:UIControlStateHighlighted];
    
    [self addName:name onButton:btn withFrame:rect];
}

-(void) addName: (NSString *) name onButton: (UIButton *) btn withFrame:(CGRect) rect {
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, rect.size.height - 30, rect.size.width, 30)];
    [v setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.45]];
    [v addGestureRecognizer:[self getTapGuesture]];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, 30)];
    v.tag = 7;
    lbl.tag = 8;
    lbl.text = [self createName:name];
    lbl.textColor = [UIColor whiteColor];
    [v addSubview:lbl];
    [btn addSubview:v];
}

-(NSString *) createName: (NSString *) name {
    NSRange range = [name rangeOfString:@"_"];
    NSString *text;
    if (range.location == NSNotFound) {
        text = name;
    } else {
        text = [name substringFromIndex: range.location];
    }
    text = [text stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    text = [text stringByReplacingOccurrencesOfString:@".jpg" withString:@""];    
    return [text capitalizedString];
}

#pragma mark -
#pragma mark event handlers
-(void) startSelectingImages: (UILongPressGestureRecognizer *) longPress {
    
    if (longPress.state == UIGestureRecognizerStateBegan) {
        imageSelectMode = YES;
        share.hidden = NO;
        delete.hidden = NO;
        back.hidden = YES;
        cancelSelected.hidden = NO;
        
        UIButton * btn = (UIButton *) longPress.view;
        [self imageTapped:btn];
    }
}

-(void) backPressed {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) deletePressed {
    [self deleteImages];
//    if (SYSTEM_VERSION_LESS_THAN(@"9.0")) {
//
//        UIAlertView *alert = [[UIAlertView alloc]
//                              initWithTitle: @""
//                              message: @"Are you sure that you want to delete these images ?"
//                              delegate: self
//                              cancelButtonTitle:@"OK"
//                              otherButtonTitles:@"Cancel", nil];
//        [alert show];
//    } else {
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure that you want to delete these images ?" preferredStyle:UIAlertControllerStyleAlert];
//
//        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
//                             {
//                                 [self deleteImages];
//                             }];
//        [alert addAction:ok];
//
//        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
//                             {
//
//                             }];
//        [alert addAction:cancel];
//        [self presentViewController:alert animated:YES completion:nil];
//    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self deleteImages];
    }
}

-(void) sharePressed {
    
    UIImage *image;
    NSMutableArray *activityItems = [[NSMutableArray alloc] init];
    for (int i = 0; i < selectedImages.count; i++) {
        NSInteger index = [[selectedImages objectAtIndex:i] integerValue];
        NSString *path = [imagesInGallery objectAtIndex:index];
        NSString *imagePath = [NSString stringWithFormat:@"%@/%@", dataPath, path];
        image = [UIImage imageWithContentsOfFile:imagePath];
        [activityItems addObject:image];
    }
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    [self presentViewController:activityViewControntroller animated:true completion:nil];
    
}

-(void) deleteImages {
    // delete all images
    // reload the gallery
    NSFileManager *manager = [NSFileManager defaultManager];
    NSError *error;
    for (int i = 0; i < selectedImages.count; i++) {
        NSInteger index = [[selectedImages objectAtIndex:i] integerValue];
        NSString *path = [imagesInGallery objectAtIndex:index];
        NSString *imagePath = [NSString stringWithFormat:@"%@/%@", dataPath, path];
        
        [manager removeItemAtPath:imagePath error:&error];
        
        if (error) {
            NSLog(@"file at path : %@, was not deleted", imagePath);
            NSLog(@"%@", error);
        }
        [galleryButtons removeObjectAtIndex:index];
    }
    
    imageSelectMode = NO;
    share.hidden = YES;
    delete.hidden = YES;
    cancelSelected.hidden = YES;
    back.hidden = NO;
    
    [selectedImages removeAllObjects];
    [self clearGallery];
    [self getAllImagesFromLocation];    
    [self buildTheGalleryWithTotalFiles];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def removeObjectForKey:@"LAST_IMAGE"];
    [def synchronize];
}

-(void) clearGallery {
    if (scroll == nil) {
        return;
    }
    NSLog(@"woill clear gallery");
    for (UIView *v in scroll.subviews) {
        [v removeFromSuperview];
    }
    [imagesInGallery removeAllObjects];
}

-(IBAction) imageTapped: (UIButton *) btn {
    if (imageSelectMode) {
        [self addRemoveCheckedIndicatorOnButton:btn];
    } else {
        [selectedImages addObject:[NSNumber numberWithInteger:btn.tag]];
        NSString *imagePath = [NSString stringWithFormat:@"%@/%@", dataPath, [imagesInGallery objectAtIndex:btn.tag]];
        UIImage *img = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@",imagePath]];
        fullScreen = [[UIImageView alloc] initWithImage:img];
        [fullScreen setUserInteractionEnabled:YES];
        [fullScreen setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [self.view addSubview:fullScreen];
        
        UIView *vv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 70)];
        [vv setBackgroundColor:[UIColor blackColor]];
        
        UILabel *ttl = [[UILabel alloc] initWithFrame:CGRectMake(5, 20, self.view.frame.size.width - 10, 30)];
        ttl.textAlignment = NSTextAlignmentLeft;
        ttl.numberOfLines = 0;
        [ttl setFont:[UIFont systemFontOfSize:18.0]];
        [ttl setTextColor:[UIColor whiteColor]];
        [ttl setText:[self createName:[NSString stringWithFormat:@"%@", [imagesInGallery objectAtIndex:btn.tag]]]];
        
        
        UIButton *share2 = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 100, 10, 50, 50)];        
        [self setImage:@"share_img" forButton:share2];
        [share2 addTarget:self action:@selector(sharePressed) forControlEvents:UIControlEventTouchUpInside];
        [vv addSubview:share2];
        
        UIButton *cam = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 50, 10, 50, 50)];
        [self setImage:@"ic_cam" forButton:cam];
        [cam addTarget:self action:@selector(openCamera) forControlEvents:UIControlEventTouchUpInside];
        [vv addSubview:cam];
        
        UIButton *x = [[UIButton alloc] initWithFrame:CGRectMake(5, 20, 30, 30)];
        [self setImage:@"back_img" forButton:x];
        [x.titleLabel setFont:[UIFont systemFontOfSize:40.0]];
        [x addTarget:self action:@selector(closeFullScreen) forControlEvents:UIControlEventTouchUpInside];
        [vv addSubview:x];
        
        [fullScreen addSubview:vv];
        [fullScreen bringSubviewToFront:vv];
        
        UIView *vvb = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 70, self.view.frame.size.width, 70)];
        [vvb setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.45]];
        [vvb addSubview:ttl];
        
        [fullScreen addSubview:vvb];
        [fullScreen bringSubviewToFront:vvb];
    }
}

-(void) addRemoveCheckedIndicatorOnButton: (UIButton *) btn {
    UIView *t = [self isButtonChecked:btn];
    if (t != nil) {
        [t removeFromSuperview];
        [self decreaseTitleView:btn];
        [self removeFromArray:(int)btn.tag];
    } else {
        [selectedImages addObject:[NSNumber numberWithInteger:btn.tag]];
        UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"accept_img" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
        img.frame = CGRectMake(btn.frame.size.width - 35, 5, 30, 30);
        img.tag = 55;
        [btn addSubview: img];
        [self increaseTitleView:btn];
    }
}

-(UITapGestureRecognizer *) getTapGuesture {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
    tapRecognizer.numberOfTapsRequired = 1;
    return tapRecognizer;
}

-(void) didTap:(UITapGestureRecognizer *) tap {
    if ([tap.view.superview isKindOfClass:[UIButton class]]) {
        UIButton *tapBtn = (UIButton *)tap.view.superview;
        [self imageTapped:tapBtn];
    }
}

-(void) removeFromArray: (int) tag {
    for (int i = 0; i < selectedImages.count; i++) {
        if ([[selectedImages objectAtIndex:i] intValue] == tag) {
            [selectedImages removeObjectAtIndex:i];
            break;
        }
    }
}

-(void) increaseTitleView: (UIButton *) btn {
    for (UIView *v in btn.subviews) {
        if (v.tag == 7) {
            [v setUserInteractionEnabled:YES];
            [v setFrame:CGRectMake(0, 0, btn.frame.size.width, btn.frame.size.height)];
            [[v.subviews objectAtIndex:0] setFrame:CGRectMake(0, v.frame.size.height - 30, v.frame.size.width, 30)];
            break;
        }
        
    }
}

-(void) decreaseTitleView: (UIButton *) btn {
    for (UIView *v in btn.subviews) {
        if (v.tag == 7) {
            [v setFrame:CGRectMake(0, btn.frame.size.height - 30, btn.frame.size.width, 30)];
            [[v.subviews objectAtIndex:0] setFrame:CGRectMake(0, 0, v.frame.size.width, 30)];
            break;
        }
    }
}

-(UIView *) isButtonChecked: (UIButton *) btn {
    for (UIView *v in btn.subviews) {
        if (v.tag == 55) {
            return v;
        }
    }
    return nil;
}

-(void) openCamera {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) closeFullScreen {
    [selectedImages removeAllObjects];
    [fullScreen removeFromSuperview];
    fullScreen = nil;
}

-(void) clearSelected {
    imageSelectMode = NO;
    share.hidden = YES;
    delete.hidden = YES;
    cancelSelected.hidden = YES;
    back.hidden = NO;
    [selectedImages removeAllObjects];
    for (UIButton* b in galleryButtons) {
        UIView *t = [self isButtonChecked:b];
        if (t != nil) {
            [t removeFromSuperview];
            [self decreaseTitleView:b];
        }
    }
}

- (UIImage *)croppIngimageByImageName:(UIImage *)imageToCrop toRect:(CGRect)rect
{
    //CGRect CropRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height+15);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

@end
