//
//  CameraViewController.m
//  ImageRecognitionFramework
//
//  Created by Aleksandar Mitrovski on 6/7/18.
//  Copyright © 2018 Aleksandar Mitrovski. All rights reserved.
//

#import "CameraViewController.h"

@interface CameraViewController () {
    UITapGestureRecognizer *tapRecognizer;
    UIPinchGestureRecognizer *zoomRecognizer;
    CALayer *drawLayer;
    
    UIView *vbottom;
    
    float zoom;
}

@property (nonatomic, strong) UIButton *noFlashBtn;
@property (nonatomic, strong) UIButton *setFlashBtn;
@property (nonatomic, strong) UIButton *autoFlashBtn;
@property (nonatomic, strong) UIButton *flashToggleBtn;
@property (nonatomic, strong) UIButton *galleryBtn;

@property AVCaptureDevice *photoDevice;
@property AVCaptureSession *session;
@property AVCaptureDeviceInput *input;
@property AVCaptureStillImageOutput *output;
@property AVCaptureVideoPreviewLayer *previewLayer;
@end

@implementation CameraViewController


@synthesize preview = _preview;
@synthesize gallery = _gallery;
@synthesize phoneNum = _phoneNum;
@synthesize lng = _lng;

#pragma mark -
#pragma mark Manage View Initialization
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initGestures];
    
    [self initCamera];
    [self addButtons];
    
    zoom = 1.0;
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [super viewWillDisappear:animated];
}

-(void) viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [super viewWillAppear:animated];
    
    [self buildGalleryButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (bool) shouldAutorotate {
    return NO;
}

-(UIInterfaceOrientationMask) supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark -
#pragma mark Set Gesture recognizers
- (void) initGestures {
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToFocus:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapRecognizer];
    
    zoomRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(zoomImg:)];
    [self.view addGestureRecognizer:zoomRecognizer];
    
}
#pragma mark -
#pragma mark Init Camera

- (void) initCamera {
    NSArray *photoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    
    AVCaptureDevicePosition position = AVCaptureDevicePositionBack;
    
    for (AVCaptureDevice *device in photoDevices) {
        if (device.position == position) {
            _photoDevice = device;
            break;
        }
    }
    
    if (_photoDevice) {
        if ([_photoDevice isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeAutoWhiteBalance]) {
            [_photoDevice setWhiteBalanceMode:AVCaptureWhiteBalanceModeAutoWhiteBalance];
        }
        NSError *errorFl = nil;
        if (!errorFl) {
            [_photoDevice lockForConfiguration:&errorFl];
        } else {
            NSLog(@"%@", errorFl);
        }
        [_photoDevice setFlashMode:AVCaptureFlashModeOff];
        [_photoDevice unlockForConfiguration];
        
        NSError * error = nil;
        _input = [ AVCaptureDeviceInput deviceInputWithDevice: _photoDevice error: &error ];
        
        NSString *preset = AVCaptureSessionPreset1920x1080;//AVCaptureSessionPresetPhoto;
        if (![_photoDevice supportsAVCaptureSessionPreset:preset])
        {
            if ([_photoDevice supportsAVCaptureSessionPreset: AVCaptureSessionPreset640x480]) {
                preset = AVCaptureSessionPreset640x480;
            } else if ([_photoDevice supportsAVCaptureSessionPreset: AVCaptureSessionPreset1280x720]) {
                preset = AVCaptureSessionPreset1280x720;
            } else if ([_photoDevice supportsAVCaptureSessionPreset: AVCaptureSessionPresetMedium]) {
                preset = AVCaptureSessionPresetMedium;
            } else {
                NSLog(@"%@", [NSString stringWithFormat:@"Capture session preset not supported by video device: %@", preset]);
                return;
            }
        }
        
        // create the capture session
        _session = [[AVCaptureSession alloc] init];
        _session.sessionPreset = preset;
        
        
        if ([_session canAddInput:_input]) {
            [_session addInput:_input];
        }
        
        _previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_session];
        [_previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        CALayer *rootLayer = [[self view] layer];
        [rootLayer setMasksToBounds:YES];
        [_previewLayer setFrame:self.view.frame];
        [rootLayer insertSublayer:_previewLayer atIndex:0];
        
        
        _output = [[AVCaptureStillImageOutput alloc] init];
        NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:AVVideoCodecJPEG, AVVideoCodecKey, nil];
        [_output setOutputSettings:outputSettings];
        [_session addOutput:_output];
        
        [_session startRunning];
    }
}

- (void) addButtons {
    vbottom = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 80, self.view.frame.size.width, 80)];
    [vbottom setBackgroundColor:[UIColor blackColor]];
    
    UIButton *btn = [[UIButton alloc] init];
    [self setImage:@"cam_img" forButton:btn];
    [btn addTarget:self action:@selector(takePicture:) forControlEvents:UIControlEventTouchUpInside];
    [btn setFrame:CGRectMake(self.view.frame.size.width/2 - 33, 10, 66, 66)];
    [vbottom addSubview:btn];
    
    UIView *vTop = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    [vTop setBackgroundColor:[UIColor blackColor]];
    
    _flashToggleBtn = [[UIButton alloc] init];
    [self setImage:@"flash_off_img" forButton:_flashToggleBtn];
    [_flashToggleBtn addTarget:self action:@selector(toggleFlash:) forControlEvents:UIControlEventTouchUpInside];
    [_flashToggleBtn setFrame:CGRectMake(self.view.frame.size.width - 53, 20, 31, 30)];
    [vTop addSubview:_flashToggleBtn];
    
    _autoFlashBtn = [[UIButton alloc] init];
    _autoFlashBtn.hidden = true;
    [_autoFlashBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_autoFlashBtn setTitle:@"Auto" forState:UIControlStateNormal];
    [_autoFlashBtn setTitle:@"Auto" forState:UIControlStateSelected];
    [self setWhiteTitleColorForButton:_autoFlashBtn];
    //[self setImage:@"flash_auto_img" forButton:_autoFlashBtn];
    [_autoFlashBtn addTarget:self action:@selector(autoFlash:) forControlEvents:UIControlEventTouchUpInside];
    [_autoFlashBtn setFrame:CGRectMake(vTop.frame.size.width / 2 + 43, 20, 31, 30)];
    [vTop addSubview:_autoFlashBtn];
    
    _setFlashBtn = [[UIButton alloc] init];
    _setFlashBtn.hidden = true;
    //[self setImage:@"flash_on_img" forButton:_setFlashBtn];
    [_setFlashBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_setFlashBtn setTitle:@"On" forState:UIControlStateNormal];
    [_setFlashBtn setTitle:@"On" forState:UIControlStateSelected];
    [self setWhiteTitleColorForButton:_setFlashBtn];
    [_setFlashBtn addTarget:self action:@selector(setFlash:) forControlEvents:UIControlEventTouchUpInside];
    [_setFlashBtn setFrame:CGRectMake(vTop.frame.size.width / 2 - 15, 20, 31, 30)];
    [vTop addSubview:_setFlashBtn];
    
    _noFlashBtn = [[UIButton alloc] init];
    _noFlashBtn.hidden = true;
    [_noFlashBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
    //[self setImage:@"flash_off_img" forButton:_noFlashBtn];
    [_noFlashBtn setTitle:@"Off" forState:UIControlStateNormal];
    [_noFlashBtn setTitle:@"Off" forState:UIControlStateSelected];
    [self setYellowTitleColorForButton:_noFlashBtn];
    [_noFlashBtn addTarget:self action:@selector(noFlash:) forControlEvents:UIControlEventTouchUpInside];
    [_noFlashBtn setFrame:CGRectMake(vTop.frame.size.width / 2 - 68, 20, 31, 30)];
    [vTop addSubview:_noFlashBtn];
    
    UIButton *close = [[UIButton alloc] init];
    [self setImage:@"back_img" forButton:close];
    [close addTarget:self action:@selector(closeCamera:) forControlEvents:UIControlEventTouchUpInside];
    [close setFrame:CGRectMake(5, 15, 30, 30)];
    [vTop addSubview:close];
    
    [self.view addSubview:vTop];
    [self.view addSubview:vbottom];
}

-(void) setWhiteTitleColorForButton: (UIButton *) btn {
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
}

-(void) setYellowTitleColorForButton: (UIButton *) btn {
    [btn setTitleColor:[UIColor colorWithRed:236/255.0 green:238/255.0 blue:94/255.0 alpha:1.0] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithRed:236/255.0 green:238/255.0 blue:94/255.0 alpha:1.0] forState:UIControlStateSelected];
}

-(void) buildGalleryButton {
    if (_galleryBtn) {
        [_galleryBtn removeFromSuperview];
        _galleryBtn = nil;
    }
    _galleryBtn = [[UIButton alloc] init];    
    UIImage *galleryImg = [UIImage imageWithContentsOfFile:[self getGaleryImage]];
    
    if (galleryImg.size.width > 800) {
        galleryImg = [self croppIngimageByImageName:galleryImg toRect:CGRectMake(galleryImg.size.width/2 - 400, galleryImg.size.height/2 - 400, 800, 800)];
    } else {
        galleryImg = [self croppIngimageByImageName:galleryImg toRect:CGRectMake(galleryImg.size.width/2 - 200, galleryImg.size.height/2 - 200, 400, 400)];
    }
    [_galleryBtn setImage:galleryImg forState:UIControlStateNormal];
    [_galleryBtn setImage:galleryImg forState:UIControlStateSelected];
    [_galleryBtn addTarget:self action:@selector(openGallery:) forControlEvents:UIControlEventTouchUpInside];
    [_galleryBtn setFrame:CGRectMake(25, 15, 50, 50)];
    
    _galleryBtn.layer.cornerRadius = 5.0;
    _galleryBtn.clipsToBounds = YES;
    
    [vbottom addSubview:_galleryBtn];
}

-(NSString *) getGaleryImage {
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *imgName = [def objectForKey:@"LAST_IMAGE"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString * dataPath = [documentsDirectory stringByAppendingPathComponent:@"/MyFolder"];
    
    if (imgName != nil) {
        return [NSString stringWithFormat:@"%@/%@", dataPath, imgName];
    } else {
        return [NSString stringWithFormat:@"%@/%@", dataPath, [self getLastImageAtPath:dataPath]];
    }
}

-(NSString *) getLastImageAtPath: (NSString *)dataPath {
    NSError *error;
    NSArray *imagesInGallery = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:&error];
    if (error) {
        NSLog(@"FAILED TO LOAD IMAGES FROM LOCATION :: %@", dataPath);
        NSLog(@"%@", error);
        return @"";
    }
    return [imagesInGallery objectAtIndex:imagesInGallery.count-1];
}

-(void) removeAllElements {
    for (UIView *v in self.view.subviews) {
        [v removeFromSuperview];
    }
    _previewLayer = nil;
    _output = nil;
    _input = nil;
    _session = nil;
    _photoDevice = nil;
}

#pragma mark -
#pragma mark Handle Events

- (IBAction)closeCamera:(UIButton *)sender {
    [self removeAllElements];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)openGallery:(UIButton *)sender {
    _gallery = [[GalleryViewController alloc] init];
    [_gallery setLang:_lng];
    [self presentViewController:_gallery animated:YES completion:nil];
}
- (IBAction)toggleFlash:(UIButton *)sender {
    [self toggleButtons];
}
- (IBAction)autoFlash:(UIButton *)sender {
    NSError *error = nil;
    if (!error) {
        [_photoDevice lockForConfiguration:&error];
    } else {
        NSLog(@"%@", error);
    }
    [_photoDevice setFlashMode:AVCaptureFlashModeAuto];
    [_photoDevice unlockForConfiguration];
    [self toggleButtons];
    [self setFlashImageForStatus:@"auto"];
    
    [self setYellowTitleColorForButton:_autoFlashBtn];
    [self setWhiteTitleColorForButton:_setFlashBtn];
    [self setWhiteTitleColorForButton:_noFlashBtn];
}
- (IBAction)setFlash:(UIButton *)sender {
    NSError *error = nil;
    if (!error) {
        [_photoDevice lockForConfiguration:&error];
    } else {
        NSLog(@"%@", error);
    }
    [_photoDevice setFlashMode:AVCaptureFlashModeOn];
    [_photoDevice unlockForConfiguration];
    [self toggleButtons];
    [self setFlashImageForStatus:@"on"];
    [self setYellowTitleColorForButton:_setFlashBtn];
    [self setWhiteTitleColorForButton:_autoFlashBtn];
    [self setWhiteTitleColorForButton:_noFlashBtn];
    
}
- (IBAction)noFlash:(UIButton *)sender {
    NSError *error = nil;
    [_photoDevice lockForConfiguration:&error];
    if (!error) {
        [_photoDevice setFlashMode:AVCaptureFlashModeOff];
    } else {
        NSLog(@"%@", error);
    }
    [_photoDevice unlockForConfiguration];
    [self toggleButtons];
    [self setFlashImageForStatus:@"off"];
    [self setYellowTitleColorForButton:_noFlashBtn];
    [self setWhiteTitleColorForButton:_setFlashBtn];
    [self setWhiteTitleColorForButton:_autoFlashBtn];
}

- (void) toggleButtons {
    NSLog(@"will toggle flash buttones");
    _autoFlashBtn.hidden = !_autoFlashBtn.hidden;
    _noFlashBtn.hidden = !_noFlashBtn.hidden;
    _setFlashBtn.hidden = !_setFlashBtn.hidden;
}

-(void) setFlashImageForStatus: (NSString *) status {
    NSString *imgName = @"";
    if ([status isEqual: @"on"]) {
        imgName = @"flash_on_img";
    } else if ([status isEqual: @"off"]) {
        imgName = @"flash_off_img";
    } else if ([status isEqual: @"auto"]) {
        imgName = @"flash_auto_img";
    }
    
    [self setImage:imgName forButton:_flashToggleBtn];
}

- (void) setImage: (NSString *) name forButton: (UIButton *) btn {
    UIImage *img = [UIImage imageNamed:name inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    
    [btn setImage:img forState:UIControlStateNormal];
    [btn setImage:img forState:UIControlStateSelected];
    [btn setImage:img forState:UIControlStateHighlighted];
}
 
- (IBAction)takePicture:(UIButton *)sender {
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in _output.connections) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo]) {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) {
            break;
        }
    }
    [_output captureStillImageAsynchronouslyFromConnection:videoConnection
        completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
            NSLog(@"%@", error);
           if (imageDataSampleBuffer != NULL) {
                NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                UIImage *image = [UIImage imageWithData:imageData];
               NSLog(@"managed to capture the image h:%f w:%f", image.size.height, image.size.width);
               self.preview = [[ImagePreviewViewController alloc] init];
               [self.preview setLng: self.lng];
               [self.preview setPhoneNum:self.phoneNum];
               [self.preview setSelectedImage:image];
               [self presentViewController:self.preview animated:YES completion:nil];
//               [self removeAllElements];
           }
    }];
}

#pragma mark -
#pragma mark handle custom camera events
- (void) tapToFocus:(UITapGestureRecognizer *) tap {
    
    NSLog(@"^^^^^ TAP WAS DETECTED");
    
    CGPoint touchPoint = [tap locationInView:self.view];
    
    NSLog(@"^^^^^ TAP POINT X: %f, Y: %f", touchPoint.x, touchPoint.y);
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    double screenWidth = screenRect.size.width;
    double screenHeight = screenRect.size.height;
    double focus_x = touchPoint.x/screenWidth;
    double focus_y = touchPoint.y/screenHeight;
    
    //[self drawRectWithCenter:touchPoint];
    
    NSError *error = nil;
    
    if (tap.state == UIGestureRecognizerStateEnded) {
        if (![_photoDevice lockForConfiguration:&error]) {
            if (error) {
                NSLog(@"%@", error);
            }
            return;
        }
        
        NSLog(@"^^^^^ WILL SET FOCUS ON POINT X: %f, Y: %f", focus_x, focus_y);
        
        [_photoDevice setFocusPointOfInterest:CGPointMake(focus_x, focus_y)];
        [_photoDevice unlockForConfiguration];
    }
}

- (void) drawRectWithCenter: (CGPoint) center {
    CGRect frame = CGRectMake(center.x - 50, center.y - 50, 100, 100);
    drawLayer = [CAShapeLayer layer];
    [drawLayer setFrame:frame];
    [drawLayer setBorderColor:[UIColor whiteColor].CGColor];
    [drawLayer setBorderWidth:2.0];
    [drawLayer setNeedsDisplay];
    
    [_previewLayer addSublayer:drawLayer];
    
    [self performSelector:@selector(removeFromLayer) withObject:nil afterDelay:1.3];
    
}

-(void) removeFromLayer {
    [drawLayer removeFromSuperlayer];
}

- (void) zoomImg: (UIPinchGestureRecognizer *) pinch {
    //_photoDevice.activeFormat.videoMaxZoomFactor
    
    float newZoom = [self getMinMaxZoomForFactor:(pinch.scale * zoom)];
    
    switch (pinch.state) {
        case UIGestureRecognizerStateBegan:
        case UIGestureRecognizerStateChanged:
            [self updateZoomForScaleFactor:newZoom];
        case UIGestureRecognizerStateEnded:
            zoom = [self getMinMaxZoomForFactor:newZoom];
            [self updateZoomForScaleFactor:zoom];
        default:
            break;
    }
}

- (float) getMinMaxZoomForFactor: (float) factor {
    // _photoDevice.activeFormat.videoMaxZoomFactor
    // 6 is the max zoom as per specifications
    return MIN(MAX(factor, 1.0), 6.0);
}

- (void) updateZoomForScaleFactor: (float) scale {
    NSError *error = nil;
    if (![_photoDevice lockForConfiguration:&error]) {
        if (error) {
            NSLog(@"%@", error);
        }
        return;
    }
    NSLog(@"current scale is :::: %f", scale);
    [_photoDevice setVideoZoomFactor:scale];
    [_photoDevice unlockForConfiguration];
}
#pragma mark -
- (UIImage *)croppIngimageByImageName:(UIImage *)imageToCrop toRect:(CGRect)rect
{
    //CGRect CropRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height+15);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}
@end
