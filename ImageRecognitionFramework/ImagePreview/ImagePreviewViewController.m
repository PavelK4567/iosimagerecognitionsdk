//
//  ImagePreviewViewController.m
//  ImageRecognitionFramework
//
//  Created by Aleksandar Mitrovski on 6/10/18.
//  Copyright © 2018 Aleksandar Mitrovski. All rights reserved.
//

#import "ImagePreviewViewController.h"

@interface ImagePreviewViewController () {
    UIActivityIndicatorView *spinner;
    NSURLSessionDataTask *dataTask;
    UIView *v;
    UIView *bgv;
    bool hasResponse;
    
    UIButton *cancelBth;
    UIButton *saveBth;
    
    NSString *imgTranslated;
    UIView *bv;
    UIView *bv2;
}

@end

@implementation ImagePreviewViewController

@synthesize selectedImage = _selectedImage;
@synthesize phoneNum = _phoneNum;
@synthesize lng = _lng;

- (void)viewDidLoad {
    [super viewDidLoad];
    hasResponse = false;
    // Do any additional setup after loading the view.
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [super viewWillDisappear:animated];
}

-(void) viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [super viewWillAppear:animated];
    [self buildScreen];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark build screen

-(void) buildScreen {
    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height)];
    img.contentMode = UIViewContentModeScaleAspectFit;
    [img setBackgroundColor:[UIColor blackColor]];
    [img setImage:_selectedImage];
    [self.view addSubview:img];
    
    UIView *vTop = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    [vTop setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:vTop];
    
    UIButton *close = [[UIButton alloc] init];
    [self setImage:@"back_img" forButton:close];
    [close addTarget:self action:@selector(handleCancel) forControlEvents:UIControlEventTouchUpInside];
    [close setFrame:CGRectMake(5, 15, 30, 30)];
    [vTop addSubview:close];
    
    bgv = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 70, self.view.frame.size.width, 70)];
    [bgv setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:bgv];
    
    cancelBth = [[UIButton alloc] init];
    [cancelBth setFrame:CGRectMake(self.view.frame.size.width/2 - 80, 5, 60, 60)];
    [self setImage:@"decline_img" forButton:cancelBth];
    [cancelBth addTarget:self action:@selector(handleCancel) forControlEvents:UIControlEventTouchUpInside];
    [bgv addSubview:cancelBth];
    
    saveBth = [[UIButton alloc] init];
    [saveBth setFrame:CGRectMake(self.view.frame.size.width/2 + 10, 5, 60, 60)];
    [self setImage:@"accept_img" forButton:saveBth];
    [saveBth addTarget:self action:@selector(handleSave) forControlEvents:UIControlEventTouchUpInside];
    [bgv addSubview:saveBth];
    
}

- (void) setImage: (NSString *) name forButton: (UIButton *) btn {
    UIImage *img = [UIImage imageNamed:name inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    
    [btn setImage:img forState:UIControlStateNormal];
    [btn setImage:img forState:UIControlStateSelected];
    [btn setImage:img forState:UIControlStateHighlighted];
}

-(void) removeAllElements {
    for (UIView *v in self.view.subviews) {
        [v removeFromSuperview];
    }
    _selectedImage = nil;
}

#pragma mark -
#pragma mark event handlers

- (void) handleCancel {
    [self removeAllElements];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) handleSave {
    
    bgv.hidden = YES;    
    
    CGRect frame = [UIScreen mainScreen].bounds;
    v = [[UIView alloc] initWithFrame:frame];
    v.backgroundColor = [UIColor colorWithRed:(0.0/255.0) green:0.0/255.0 blue:(0.0/255.0) alpha:0.45];
    
    [self initSpinnerOnView:v];
    [[UIApplication sharedApplication].keyWindow addSubview:v];
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:v];
    
    [self sendImageToServer];
}

#pragma mark -
#pragma mark spinner methods
-(void)initSpinnerOnView: (UIView *) v{
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    spinner.hidesWhenStopped = YES;
    
    UILabel *txt = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 125, self.view.frame.size.height/2 + 30, 250, 60)];
    [txt setNumberOfLines:0];
    [txt setFont:[UIFont systemFontOfSize:20.0]];
    [txt setTextAlignment:NSTextAlignmentCenter];
    [txt setBackgroundColor:[UIColor clearColor]];
    [txt setText:@"Processando o reconhecimento de imagem..."];
    [txt setTextColor:[UIColor whiteColor]];
    
    [v addSubview:spinner];
    [v addSubview:txt];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [spinner startAnimating];
}
-(void)stopSpinner{
    if(spinner != nil){
        if([spinner isAnimating]){
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            [spinner stopAnimating];
            [spinner removeFromSuperview];
            if (v != nil) {
                [v removeFromSuperview];
            }
        }
    }
}
#pragma mark -
#pragma mark prepare image for processing
-(NSString *) buildURL {
    UIDevice *device = [UIDevice currentDevice];
    NSString *uniqueIdentifier = [[device identifierForVendor] UUIDString];
    NSString *baseUrl = @"http://pi-1.kantoo.com:8001/PitionaryProxy/DoCamFindRequestNoStateChecking?groupID=5";
    NSString *url = [NSString stringWithFormat:@"%@&msisdn=%@&deviceID=%@&language=%@", baseUrl, _phoneNum, uniqueIdentifier, _lng];
    return url;
}

-(void) sendImageToServer {
    NSLog(@"%@", [self buildURL]);
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[self buildURL]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"image/jpeg" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPBody:UIImageJPEGRepresentation([self imageRotatedByDegrees:_selectedImage deg:90.0], 1.0)];
    
    //[self performSelector:@selector(cancelRequest) withObject:nil afterDelay:10.0];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        self->hasResponse = true;
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSLog(@"%ld", (long)httpResponse.statusCode);
        if (httpResponse.statusCode == 200) {
            NSError *parseError = nil;
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                        
            //UIImageWriteToSavedPhotosAlbum(self->_selectedImage, nil,  nil ,nil);
            self->imgTranslated = [[[responseDictionary objectForKey:@"tags"] objectAtIndex:0] objectForKey:@"translate"];
            self->imgTranslated = [self->imgTranslated stringByReplacingOccurrencesOfString:@" " withString:@"_"];
            [self->imgTranslated stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSLog(@"%@", [[[responseDictionary objectForKey:@"tags"] objectAtIndex:0] objectForKey:@"translate"]);
            // save image on app bundle
            //[self saveImageOnAppDir:imgTranslated];
            // show info
            dispatch_async(dispatch_get_main_queue(), ^{
                [self stopSpinner];
                //[self handleCancel];
                [self displayResult:self->imgTranslated];
            });
        } else {
            NSLog(@"Error:: %@", error);
            // show error -
            dispatch_async(dispatch_get_main_queue(), ^{
                [self stopSpinner];
                [self displayError];
            });
            
        }
    }];
    [dataTask resume];
}

- (void) displayError {
    [[[UIAlertView alloc] initWithTitle:@"" message:@"Desculpe, ocorreu um erro. Por favor, fotografe novamente." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self handleCancel];
}

-(void) displayResult: (NSString *) text {
    
    UIView *v = [[UIView alloc] init];
    [v setFrame:CGRectMake(0, 0, self.view.frame.size.width, 70)];
    [v setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0]];
    
    UIButton *cancelSelected = [[UIButton alloc] initWithFrame:CGRectMake(5, 20, 30, 30)];
    [self setImage:@"back_img" forButton:cancelSelected];
    [cancelSelected.titleLabel setFont:[UIFont systemFontOfSize:40.0]];
    [cancelSelected setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelSelected addTarget:self action:@selector(saveImageWithName) forControlEvents:UIControlEventTouchUpInside];
    [v addSubview:cancelSelected];
    
    UIButton *share2 = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 100, 10, 50, 50)];
    [self setImage:@"share_img" forButton:share2];
    [share2 addTarget:self action:@selector(sharePressed) forControlEvents:UIControlEventTouchUpInside];
    [v addSubview:share2];
    
    UIButton *cam = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 50, 10, 50, 50)];
    [self setImage:@"ic_cam" forButton:cam];
    [cam addTarget:self action:@selector(saveImageWithName) forControlEvents:UIControlEventTouchUpInside];
    [v addSubview:cam];
    
    [self.view addSubview:v];
    [self.view bringSubviewToFront:v];
    
    bv = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 60, self.view.frame.size.width, 60)];
    [bv setBackgroundColor:[UIColor colorWithRed:61/255.0 green:61/255.0 blue:61/255.0 alpha:1.0]];
    
    bv2 = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 120, self.view.frame.size.width, 60)];
    [bv2 setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.45]];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, self.view.frame.size.width - 10, 30)];
    lbl.text = [self createName:text];
    [lbl setFont:[UIFont systemFontOfSize:18.0]];
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.textColor = [UIColor whiteColor];
    [lbl setNumberOfLines:0];
    [bv2 addSubview:lbl];
    
    UILabel *lbl2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 15, 200, 30)];
    lbl2.text = @"A imagem foi salva";
    lbl2.textColor = [UIColor whiteColor];
    [bv addSubview:lbl2];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(bv.frame.size.width - 100, 0, 100, 60)];
    [btn setTitle:@"Desfazer" forState:UIControlStateNormal];
    [btn setTitle:@"Desfazer" forState:UIControlStateSelected];
    [btn setTitleColor:[UIColor colorWithRed:145/255.0 green:201/255.0 blue:196/255.0 alpha:1.0] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithRed:145/255.0 green:201/255.0 blue:196/255.0 alpha:1.0] forState:UIControlStateSelected];
    //[btn setBackgroundColor:[UIColor colorWithRed:205/255.0 green:205/255.0 blue:205/255.0 alpha:0.9]];
    [btn addTarget:self action:@selector(handleCancel) forControlEvents:UIControlEventTouchUpInside];
    [bv addSubview:btn];
    
    [self.view addSubview:bv];
    [self.view bringSubviewToFront:bv];
    
    [self.view addSubview:bv2];
    [self.view bringSubviewToFront:bv2];
    
    [self performSelector:@selector(clearSubView) withObject:nil afterDelay:3.0];
}

-(void) sharePressed {    
    UIImage *image = _selectedImage;
    NSMutableArray *activityItems = [[NSMutableArray alloc] init];
    [activityItems addObject:image];
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    [self presentViewController:activityViewControntroller animated:true completion:nil];
    
}

-(NSString *) createName: (NSString *) name {
    NSRange range = [name rangeOfString:@"_"];
    NSString *text;
    if (range.location == NSNotFound) {
        text = name;
    } else {
        text = [name substringFromIndex: range.location];
    }
    text = [text stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    text = [text stringByReplacingOccurrencesOfString:@".jpg" withString:@""];    
    return [text capitalizedString];
}

-(void) clearSubView {
    [UIView animateWithDuration:0.5 animations:^{
        [self->bv setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 60)];
        [self->bv2 setFrame:CGRectMake(0, self.view.frame.size.height - 60, self.view.frame.size.width, 60)];
    } completion:^(BOOL finished) {
        [self->bv removeFromSuperview];
    }];
}

-(void) saveImageWithName {
    UIImageWriteToSavedPhotosAlbum(_selectedImage, nil,  nil ,nil);
    [self saveImageOnAppDir:imgTranslated];
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(nullable NSError *)error {
    NSLog(@"error &&&& %@", error);
}

-(void) cancelRequest {
    if (!hasResponse) {
        [dataTask cancel];
        [self stopSpinner];
    }
}

-(bool) shouldResizeImage {
    NSLog(@"_selectedImage.size.width = %f", _selectedImage.size.width);
    return _selectedImage.size.width > 1024;
}

-(UIImage *) resizeImage {
    float delta = [self getImageSizeRelation];
    UIGraphicsBeginImageContext(CGSizeMake(1024, _selectedImage.size.height * delta));
    [[self imageRotatedByDegrees:_selectedImage deg:90.0] drawInRect:CGRectMake(0, 0, 1024, _selectedImage.size.height * delta)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

-(float) getImageSizeRelation {
    return _selectedImage.size.width / 1024;
}

#pragma mark -
#pragma mark save image on app directory
-(void) saveImageOnAppDir: (NSString *) name {
    NSError *error;//[self imageRotatedByDegrees:_selectedImage deg:90.0]
    NSData *imageData = UIImagePNGRepresentation([self fixOrientationOfImage:_selectedImage]);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", name]];
    
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/MyFolder"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"MyFolder/%@.jpg", name]];
    
    NSLog(@"pre writing to file");
    if (![imageData writeToFile:imagePath atomically:NO])
    {
        NSLog(@"Failed to cache image data to disk");
    }
    else
    {
        NSLog(@"the cachedImagedPath is %@",imagePath);
        [self setLastSavedImage:[NSString stringWithFormat:@"%@.jpg", name]];
        [self setDateTakenAndIncreaseCount];
        [self handleCancel];
    }
}

- (UIImage *)fixOrientationOfImage:(UIImage *)image {
    
    // No-op if the orientation is already correct
    if (image.imageOrientation == UIImageOrientationUp) return image;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

- (UIImage *)imageRotatedByDegrees:(UIImage*)oldImage deg:(CGFloat)degrees{
    
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,oldImage.size.width, oldImage.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(degrees * M_PI / 180);
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    CGContextRotateCTM(bitmap, (degrees * M_PI / 180));
    
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-oldImage.size.width / 2, -oldImage.size.height / 2, oldImage.size.width, oldImage.size.height), [oldImage CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void) setLastSavedImage: (NSString *) name {
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:name forKey:@"LAST_IMAGE"];
    [def synchronize];
}

-(void) setDateTakenAndIncreaseCount {
    NSUserDefaults *standard = [NSUserDefaults standardUserDefaults];
    NSDate *date = (NSDate *)[standard objectForKey:@"dateTaken"];
    if (date == nil) {
        [standard setInteger:1 forKey:@"imagesTaken"];
        [standard setObject:[NSDate date] forKey:@"dateTaken"];
    } else {
        int count = [standard integerForKey:@"imagesTaken"];
        
        NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:date];
        
        int numberOfDays = secondsBetween / 86400;
        if (numberOfDays > 30) {
            [standard setInteger:1 forKey:@"imagesTaken"];
            [standard setObject:[NSDate date] forKey:@"dateTaken"];
        } else {
            count += 1;
            [standard setInteger:count forKey:@"imagesTaken"];
        }
    }
    [standard synchronize];
}

@end
