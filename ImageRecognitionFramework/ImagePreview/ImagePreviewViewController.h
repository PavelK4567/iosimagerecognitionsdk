//
//  ImagePreviewViewController.h
//  ImageRecognitionFramework
//
//  Created by Aleksandar Mitrovski on 6/10/18.
//  Copyright © 2018 Aleksandar Mitrovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePreviewViewController : UIViewController<NSURLSessionTaskDelegate, UIAlertViewDelegate>

@property (nonatomic, retain) UIImage *selectedImage;

@property (nonatomic, copy) NSString *phoneNum;
@property (nonatomic, copy) NSString *lng;

@end
